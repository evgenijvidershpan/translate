/* This file is a part of translation toolset "translate".
 Copyright © 2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MSGTABLE_H_INCLUDED
#define MSGTABLE_H_INCLUDED

#include <windows.h>

/**
@brief   Главный "обьект" локализатора
@details данный обьект используется для доступа ко всем кодам сообщений.
         пример получения локализированой строки для заголовка приложения:
         SetWindowText(hMainWindow, TR::DO(TR::MAIN_TITLE);
         при желании можно определить макрос
         ``#define TRDO(msg) TR::DO(TR::msg)``
         но так вы потеряете преимущества подсказок и автодополнения от IDE.
*/
typedef struct TR {
enum ID {
HELLOMSG = 0x4289, //!< "Hello World!"
BYEMSG = 0xA5A3, //!< "Bye!"
};

/**
@brief   Функция которая собственно и добывает строку перевода по коду сообщения
@details эту функцию вы должны реализовать сами (например обычный LoadString)
         которому даже fail-back писать не прийдётся, строки интегрированы же!

         Метод реализации локализации:
         программисты работающие над проектом по мере необходимости
         "резервируют" новые константы в один PUSH добавляя константы в файл
         msgtable.h (единственный для каждого проекта) по шаблону
         ``HELLOMSG = 35345, //!< "msg"`` затем вносят в translate.cpp
         (единственный для каждого проекта) соответствующие записи
         в fail-back функцию (например добавляют case для нового кода)
         после применения PUSH (записи в репозитории) просто используют TR::DO
         вместо сообщений (fail-back гарантирует строковую константу)
         отдел переводов при подготовке к релизу берёт файл msgtable.h и переводит
         этот файл на все нужные языки, файлы перевода интегрируют в приложение
         так чтобы в зависимости от настроек приложения функция TR::DO могла
         получить по коду нужную строку из нужного перевода.
*/
static LPCTSTR DO(TR::ID msgID);
static LPCTSTR ORIGINAL(TR::ID msgID);
} TR;

#endif // MSGTABLE_H_INCLUDED
