/* This file is a part of translation toolset "translate".
 Copyright © 2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef TRANSLATE_H_INCLUDED
#define TRANSLATE_H_INCLUDED

/**
@brief Грязный хак позволяющий отменить построение автоматических зависимостей
@details
Поскольку в файле msgtable.h будут определяться только "вечные" константы, то при
его изменении, необходимости в рекомпиляции всех модулей его использующих нет.
для начала использования translate скопируйте и иодифицируйте файлы msgtable.h
msgtable.cpp под ваши строки.
*/
#define INCLUDE_CONST(M) #M
#include INCLUDE_CONST(../msgtable.h)

/**
@brief Версия перевода (увеличивается на 1 при каждой чистке таблицы кодов)
@details
При чистке таблицы часть кода может прийти в некомпилируемое состояние,
поэтому обновление версии даст нам гарантию перекомпиляции зависящих от
translate.h файлов.
Напомню что грязный хак выше блокирует построение зависимостей к таблице.
*/
#define TRANSLATE_VERSION 1

#endif // TRANSLATE_H_INCLUDED
